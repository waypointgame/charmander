import * as React from 'react';

import { Provider } from 'react-redux';
import { store } from './state';

import { Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';

import NavBar from './navbar';
import Home from './views/home';
import Profile from './views/profile';
import { BackgroundSpinner } from './signposts';

import './App.css';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <BrowserRouter>
          <div className="App">
            <NavBar />
            <div className="View">
              <Route exact={true} path="/" component={Home} />
              <Route path="/profile" component={Profile} />
            </div>
            <BackgroundSpinner />
          </div>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;
