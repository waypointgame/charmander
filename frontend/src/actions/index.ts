import * as Redux from 'redux';

// Eventually will be Redux.Action<string>
export class Action implements Redux.Action {
    type: string;
}

export class PayloadAction<T> extends Action {
    payload: T;
}
