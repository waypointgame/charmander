import * as React from 'react';
import { Dispatch, bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Route } from 'react-router';
import { Link } from 'react-router-dom';
import * as moment from 'moment';
import { StateProps, ProfileProps } from '../state';
import { Eventually, may_spin } from '../signposts';
import { Action } from '../actions';
import { LoadProfile } from '../reducers/profile';
import { EditNameForm } from '../components/username-form';
import { PasswordForm } from '../components/password-form';

import { History } from 'history';

type Props = {
  profile?: Eventually<ProfileProps>,
  onLoad?: () => void,
};

// This would be a SFC, but I need componentDidMount()
class Profile extends React.PureComponent<Props, {}> {
  componentDidMount() {
    if (this.props.onLoad) {
      this.props.onLoad();
    }
  }
  formatDateTime(timestamp: number) {
    return moment.unix(timestamp).utc().format('llll');
  }
  render() {
    return (
      <React.Fragment>
        <div className="App-header">
          <h2>User Profile</h2>
        </div>
        {may_spin<ProfileProps>(this.props.profile, (profile) => 
          <React.Fragment>
            <Route 
              path="/profile/change-name" 
              render={({ history }) => (
                <EditNameForm 
                  initialValues={profile}
                  onSubmit={() => { 
                    (history as History).push('/profile');
                    this.componentDidMount(); // Reload
                  }}
                />
              )} 
            />
            <Route 
              path="/profile/password" 
              render={({ history }) => (
                <PasswordForm />
              )} 
            />
            <dl>
              <dt>Name</dt>
              <dd>{profile.name}</dd>
              <dd><Link to="/profile/change-name">Update</Link></dd>
              <dt>Email</dt>
              <dd>{profile.email}</dd>
              <dt>Date Joined</dt>
              <dd>{this.formatDateTime(profile.date_joined)}</dd>
            </dl>
            {
              // <Link to="/profile/password">Change Password</Link>
            }
            <a href="/account/password_change/">Change Password</a>
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default connect<Props, {}>(
  // Redux State -> Props
  (state: StateProps) => {
    return {
      profile: state.profile
    };
  },
  // Dispatch -> props
  dispatch => 
    (d: Dispatch<Action>) => bindActionCreators(
      {
          onLoad: LoadProfile.create,
      },
      d)
  // FIXME: mark as pure
)(Profile);