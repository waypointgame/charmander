import * as React from 'react';

export default function Home() {
  return (
    <React.Fragment>
      <div className="App-header">
        <h2>Home</h2>
      </div>
      <p className="App-intro">
        Stuff happens here
      </p>
    </React.Fragment>
  );
}