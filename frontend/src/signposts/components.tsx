import * as React from 'react';
import { connect } from 'react-redux';
import { PulseLoader } from 'react-spinners';
import { StateProps } from '../state';
import { BackgroundStatus, Eventually, isFailure, isSignpost, Failure } from '.';
import { CircleLoader } from 'react-spinners';

type ErrorProps = {
    error: Failure
};

export class ErrorBox extends React.Component<ErrorProps, {}> {
  render() {
    return (
        <dialog className="error" {...{open: true}}>
            <h3>Error: {this.props.error.title}</h3>
            <p>{this.props.error.description}</p>
        </dialog>
    );
  }
}

export function Spinner() {
  return (
    <div style={{margin: 'auto', width: '50px'}}>
      <CircleLoader
        color={'#F5A623'}
        size={50}
      />
    </div>
  );
}

import './background-spinner.css';

const countOngoing = (backgrounds: Array<BackgroundStatus>) => {
  let count = 0;
  backgrounds.forEach((value) => {
    if (value.status === 'uncalled' || value.status === 'started') {
      count += 1;
    }
  });
  return count;
};

export const BackgroundSpinner = connect<{count: number}, {}>(
  // Redux State -> Props
  (state: StateProps) => {
    return {
      count: countOngoing(state.backgrounds)
    };
  },
)(({ count }) => {
  return (
    <div id="background-activity-spinner">
      {count 
        ? <PulseLoader
          color="#F5A623"
          size={10}
          margin="2px"
        />
        : <React.Fragment />
      }
    </div>
  );
});

export function may_spin<T>(props: Eventually<T>, func: (props: T) => JSX.Element) {
  if (props === undefined) {
    return <React.Fragment />;
  } else if (isSignpost(props)) {
    return <Spinner />;
  } else if (isFailure(props)) {
    return <ErrorBox error={props} />;
  } else {
    return func(props as T);  // FIXME: I though typescript could infer this automatically?
  }
}
