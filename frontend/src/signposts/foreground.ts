import { Dispatch } from 'redux';
import { Action, PayloadAction } from '../actions';

export type Signpost = {
    $signpost: 'signpost',
};

export type Failure = {
    $failure: 'failure',
    title: string,
    description: string,
};

export function isSignpost(obj: Signpost | {}): obj is Signpost {
    if (obj === undefined) {
        return false;
    } else {
        return (obj as Signpost).$signpost === 'signpost';
    }
}

export function isFailure(obj: Failure | {}): obj is Failure {
    if (obj === undefined) {
        return false;
    } else {
        return (obj as Failure).$failure === 'failure';
    }
}

export type Eventually<T> = T | Signpost | Failure | undefined;

export class RetrieveAction<ArgumentType, PayloadType> {
    readonly START: string;
    readonly FINISH: string;
    readonly FAILURE: string;

    readonly thunk: (args: ArgumentType) => Promise<PayloadType>;

    constructor(basename: string, thunk: (args: ArgumentType) => Promise<PayloadType>) {
        this.thunk = thunk;
        this.START = 'START_' + basename;
        this.FINISH = 'FINISH_' + basename;
        this.FAILURE = 'FAILURE_' + basename;
    }

    create = (args: ArgumentType) => (dispatch: Dispatch<Action>) => {
        dispatch<PayloadAction<Signpost>>({
            type: this.START,
            payload: {
                $signpost: 'signpost'
            }
        });
        this.thunk(args).then((payload: PayloadType) => {
            dispatch<PayloadAction<PayloadType>>({
                type: this.FINISH,
                payload: payload,
            });
        }).catch((error: Failure) => {
            dispatch<PayloadAction<Failure>>({
                type: this.FAILURE,
                payload: {
                    $failure: 'failure',
                    ...error
                },
            });
        });
    }

    reduceStart = (state: Eventually<PayloadType>, action: PayloadAction<Signpost>) => {
        return action.payload;
    }

    reduceFinish = (state: Eventually<PayloadType>, action: PayloadAction<PayloadType>) => {
        return action.payload;
    }

    reduceError = (state: Eventually<PayloadType>, action: PayloadAction<Failure>) => {
        return action.payload;
    }

    reduce = (state: Eventually<PayloadType>, action: Action): Eventually<PayloadType> => {
        switch (action.type) {
            case this.START:
                return this.reduceStart(state, action as PayloadAction<Signpost>);
            case this.FINISH:
                return this.reduceFinish(state, action as PayloadAction<PayloadType>);
            case this.FAILURE:
                return this.reduceError(state, action as PayloadAction<Failure>);
            default:
                return state;
        }
    }
}
