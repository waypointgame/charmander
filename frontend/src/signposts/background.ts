/*

This keeps track of background tasks that we don't really care about the results,
as long as it succeeds and the user knows we're doing something.

This is especially built for frequently-fired requests, eg continuous saving.

*/
import { Dispatch } from 'redux';
import { Action, PayloadAction } from '../actions';
import { Failure } from '.';

export type BackgroundStatus = {
    rid: string,
    status: 'uncalled' | 'started' | 'resolved' | 'rejected',
    next?: () => void,
};

const BACKGROUND_ACTION = 'UPDATE_BACKGROUND_ACTION';

type UpdateRequestPayload = {
    rid: string,
    status: 'uncalled' | 'started' | 'resolved' | 'rejected',
    func?: () => void,
    error?: Failure,
};

export class BackgroundAction<ArgumentType> {
    readonly basename: string;
    readonly thunk: (args: ArgumentType) => Promise<{}>;

    static reduceInit = (state: Array<BackgroundStatus>, action: PayloadAction<UpdateRequestPayload>) => {
        let updated = false; // Not really functional
        let data: UpdateRequestPayload = action.payload;
        state = state.map((status: BackgroundStatus): BackgroundStatus => {
            if (status.rid === data.rid) {
                updated = true;
                if (status.status === 'resolved' || status.status === 'rejected') {
                    return {
                        rid: data.rid,
                        status: 'uncalled',
                        next: data.func,
                    };
                } else {
                    return {
                        ...status,
                        ...{next: data.func}
                    };
                }
            } else {
                return status;
            }
        });
        if (updated) {
            return state;
        } else {
            return state.concat([{
                rid: data.rid,
                status: 'uncalled',
                next: data.func,
            }]);
        }
    }

    static reduceUpdate = (state: Array<BackgroundStatus>, action: PayloadAction<UpdateRequestPayload>) => {
        let data: UpdateRequestPayload = action.payload;
        return state.map((status: BackgroundStatus): BackgroundStatus => {
            if (status.rid === data.rid) {
                return {
                    ...status,
                    ...{
                        status: data.status,
                        // XXX: This smells like a race condition
                        next: data.status === 'started' ? undefined : status.next,
                    }
                };
            } else {
                return status;
            }
        });
    }

    static reduce = (state: Array<BackgroundStatus>, action: Action): Array<BackgroundStatus> => {
        switch (action.type) {
            case BACKGROUND_ACTION:
                switch ((action as PayloadAction<UpdateRequestPayload>).payload.status) {
                    case 'uncalled':
                        return BackgroundAction.reduceInit(state, action as PayloadAction<UpdateRequestPayload>);
                    case 'started':
                    case 'resolved':
                    case 'rejected':
                        return BackgroundAction.reduceUpdate(state, action as PayloadAction<UpdateRequestPayload>);
                    default:
                        return state;
                }
            default:
                return state;
        }
    }

    static listener = (dispatch: Dispatch<Action>, state: Array<BackgroundStatus>) => {
        state.forEach((status) => {
            if (status.status === 'uncalled') {
                if (status.next) {
                    status.next();
                } else {
                    dispatch<PayloadAction<UpdateRequestPayload>>({
                        type: BACKGROUND_ACTION,
                        payload: {
                            rid: status.rid,
                            status: 'resolved',
                        },
                    });
                }
            } else if (status.next && (status.status === 'resolved' || status.status === 'rejected')) {
                status.next();
            }
        });
    }

    constructor(basename: string, thunk: (args: ArgumentType) => Promise<{}>) {
        this.thunk = thunk;
        this.basename = basename;
    }

    create = (args: ArgumentType) => (dispatch: Dispatch<Action>) => {
        dispatch<PayloadAction<UpdateRequestPayload>>({
            type: BACKGROUND_ACTION,
            payload: {
                rid: this.basename,
                status: 'uncalled',
                func: () => {
                    dispatch<PayloadAction<UpdateRequestPayload>>({
                        type: BACKGROUND_ACTION,
                        payload: {
                            rid: this.basename,
                            status: 'started',
                        },
                    });
                    this.thunk(args).then(() => {
                        dispatch<PayloadAction<UpdateRequestPayload>>({
                            type: BACKGROUND_ACTION,
                            payload: {
                                rid: this.basename,
                                status: 'resolved',
                            },
                        });
                    }).catch((error: Failure) => {
                        dispatch<PayloadAction<UpdateRequestPayload>>({
                            type: BACKGROUND_ACTION,
                            payload: {
                                rid: this.basename,
                                status: 'rejected',
                                error: error,
                            },
                        });
                    });
                }
            }
        });
    }
}
