import * as React from 'react';
import { Link } from 'react-router-dom';
import './navbar.css';

const logo = require('./logo.svg');

class NavBar extends React.Component {
  render() {
    return (
      <div className="navbar">
        <img src={logo} className="App-logo" alt="logo" />
        <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/profile">Profile</Link></li>
        </ul>
      </div>
    );
  }
}

export default NavBar;
