import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { FormStateMap } from 'redux-form';

import reducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import { Eventually, BackgroundStatus, BackgroundAction } from './signposts';

export type ProfileProps = {
    name: string,
    email: string,
    is_staff: boolean,
    is_superuser: boolean,
    date_joined: number,
    groups: Array<string>,
};

export type StateProps = {
    profile: Eventually<ProfileProps>,
    form: FormStateMap,
    backgrounds: Array<BackgroundStatus>,
};

export const store = createStore<StateProps>(reducer, /* preloadedState, */ composeWithDevTools(
    applyMiddleware(thunk)
));

store.subscribe(() => {
    let state = store.getState();
    BackgroundAction.listener(store.dispatch, state.backgrounds);
});
