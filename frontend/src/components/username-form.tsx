import * as React from 'react';
import { reduxForm, Field } from 'redux-form';
import { UpdateProfile } from '../reducers/profile';

type NameFormValues = {
  name: string,
};

export const EditNameForm = reduxForm<NameFormValues, {}>({
  form: 'profile',
  onChange: (values: NameFormValues, dispatch) => {
    console.log('On change');
    dispatch(UpdateProfile.create({name: values.name}));
  }
})((props) => {
  return (
    <dialog open={true}>
      <form onSubmit={props.handleSubmit}>
        <div>
          <label>Name: <Field name="name" component="input" type="text" /></label>
        </div>
        <button type="submit">Done</button>
      </form>
    </dialog>
  );
});
