import * as React from 'react';
import { SFC } from 'react';
import { InputHTMLAttributes } from 'react';
import { reduxForm, Field } from 'redux-form';
import { WrappedFieldProps } from 'redux-form';
// import { UpdateProfile } from '../reducers/profile';

type PasswordFormValues = {
  password1: string,
  password2: string,
};

const renderField: SFC<WrappedFieldProps & InputHTMLAttributes<HTMLInputElement>> = 
({ input, label, type, meta: { touched, error, warning } }) =>  (
  <div>
    <label>{label}: <input {...input} type={type}/></label>
    {touched ?
        <div>
        {error ? <span className="error">{error}</span> : null}
        {warning ? <span className="warning">{warning}</span> : null}
        </div>
     : null
    }}
  </div>
);

export const PasswordForm = reduxForm<PasswordFormValues, {}>({
  form: 'password',
  onSubmit : (values, dispatch, props) => {
    console.log('TODO: Submit new password');
  },
  // Deprecated, but https://github.com/DefinitelyTyped/DefinitelyTyped/issues/24013
  shouldValidate: ({ values }) => {
    console.debug('should validate', Boolean(values.password1) && Boolean(values.password2));
    return Boolean(values.password1) && Boolean(values.password2);
  },
  validate : (values, props) => {
    console.debug('validate', values.password1 === values.password2);
    if (values.password1 !== values.password2) {
      return {password2: 'Passwords do not match!'};
    } else {
      return {};
    }
  },

})((props) => {
  return (
    <dialog open={true}>
      <form onSubmit={props.handleSubmit}>
        {/* Based on the help output of AUTH_PASSWORD_VALIDATORS */}
        <ul>
          <li>Your password can&#39;t be too similar to your other personal information.</li>
          <li>Your password must contain at least 8 characters.</li>
          <li>Your password can&#39;t be a commonly used password.</li>
          <li>Your password can&#39;t be entirely numeric.</li>
        </ul>
        <Field label="Password" name="password1" component={renderField} type="password" />
        <Field label="Again" name="password2" component={renderField} type="password" />
        <button type="submit">Change</button>
      </form>
    </dialog>
  );
});
