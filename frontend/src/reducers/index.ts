import { Action } from '../actions';
import { StateProps } from '../state';
import { LoadProfile } from './profile';
import { reducer as formReducer } from 'redux-form';
import { BackgroundAction } from '../signposts';

const rootReducer = (
    state: StateProps = {
        profile: undefined,
        form: {},
        backgrounds: [],
    },
    action: Action,
) => ({
    profile: LoadProfile.reduce(state.profile, action),
    form: formReducer(state.form, action),
    backgrounds: BackgroundAction.reduce(state.backgrounds, action),
});

export default rootReducer;
