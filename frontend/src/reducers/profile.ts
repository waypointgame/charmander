import axios from 'axios';
import { AxiosResponse } from 'axios';
import { RetrieveAction, BackgroundAction } from '../signposts';
import { ProfileProps } from '../state';

export const LoadProfile = new RetrieveAction<void, ProfileProps>(
    'LOAD_PROFILE',
    () => axios({
        url: '/api/profile/',
        method: 'get',
        responseType: 'json',
        xsrfCookieName: 'csrftoken',
        xsrfHeaderName: 'X-CSRFToken',
    })
    .then((resp: AxiosResponse<ProfileProps>) => resp.data)
);

export const UpdateProfile = new BackgroundAction<{name?: string}>(
    'UPDATE_PROFILE',
    ({name}) => axios({
        url: '/api/profile/',
        method: 'patch',
        responseType: 'json',
        xsrfCookieName: 'csrftoken',
        xsrfHeaderName: 'X-CSRFToken',
        data: {
            name: name,
        }
    })
);
