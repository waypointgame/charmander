from django.http import HttpResponse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import View
from django.contrib.auth.views import PasswordChangeDoneView as auth_PasswordChangeDoneView
from django.contrib import messages
from django.shortcuts import redirect
import json


class Profile(LoginRequiredMixin, View):
    def get(self, request):
        u = request.user
        return HttpResponse(json.dumps({
            'email': u.email,
            'name': u.name,
            'is_staff': u.is_staff,
            'is_superuser': u.is_superuser,
            'date_joined': u.date_joined.timestamp(),
            'groups': [g.name for g in u.groups.all()]
        }), content_type='application/json', status=200)

    def patch(self, request):
        u = request.user
        if 'name' in request.POST:
            u.name = request.POST['name']
        u.save()
        return HttpResponse(status=204)


class PasswordChangeDoneView(auth_PasswordChangeDoneView):
    def get(self, request):
        messages.add_message(request, messages.INFO, self.title)
        return redirect('/profile', permanent=False)
