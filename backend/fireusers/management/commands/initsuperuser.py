from django.core.management.base import BaseCommand
from ...models import User


class Command(BaseCommand):
    help = 'Creates the initial superuser'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        if not User.objects.filter(is_superuser=True).count():

            User.objects.create_superuser(
                'admin@ceedeebee.com', 'changeme',
                name="Admin", is_active=True
            )

            self.stdout.write(self.style.SUCCESS('Successfully created superuser'))
