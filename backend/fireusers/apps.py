from django.apps import AppConfig


class FireusersConfig(AppConfig):
    name = 'fireusers'
