import pytest
import json


@pytest.mark.django_db()
def test_registration_form(django_user_model, client, mailoutbox):
    response = client.post('/account/register/', {
        'email': 'jdoe@test.example',
        'password1': '1two#FOUR',
        'password2': '1two#FOUR',
    })
    assert response.status_code == 200 or 300 <= response.status_code < 400

    assert len(mailoutbox) == 1
    m = mailoutbox[0]
    assert list(m.to) == ['jdoe@test.example']

    u = django_user_model.objects.get(email='jdoe@test.example')
    assert u.email == 'jdoe@test.example'
    assert not u.is_active
    assert not u.is_staff


@pytest.mark.django_db()
def test_get_profile(django_user_model, client):
    user = django_user_model(
        # FIXME: Use a data factory
        email='jdoe@test.example',
        name='John Doe',
        is_active=True,
    )
    user.save()
    client.force_login(user)
    resp = client.get('/api/profile/')
    assert resp.status_code == 200
    j = resp.json()
    assert j['email'] == 'jdoe@test.example'
    assert j['name'] == 'John Doe'


@pytest.mark.django_db()
def test_patch_profile(django_user_model, client):
    user = django_user_model(
        # FIXME: Use a data factory
        email='jdoe@test.example',
        name='John Doe',
        is_active=True,
    )
    user.save()
    client.force_login(user)
    resp = client.patch('/api/profile/', json.dumps({
        'name': 'Jane Doe'
    }), content_type='application/json')
    assert 200 <= resp.status_code < 300

    resp = client.get('/api/profile/')
    assert resp.status_code == 200
    j = resp.json()
    assert j['email'] == 'jdoe@test.example'
    assert j['name'] == 'Jane Doe'
