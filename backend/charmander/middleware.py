import json
from django.http import QueryDict
from mimeparse import parse_mime_type


class JSONMiddleware(object):
    """
    Process application/json requests data from GET, POST and PATCH requests.
    """

    def __init__(self, get_response):
        self._response = get_response

    def __call__(self, request):
        request.PUT = {}
        ct = request.META.get('CONTENT_TYPE', '')
        if not ct:
            return self._response(request)

        t, st, p = parse_mime_type(ct)
        if (t, st) == ('application', 'json') or st.endswith('json'):
            # Load the json data
            data = json.loads(request.body)
            if isinstance(data, dict):
                # For consistency, we want to return a Django QueryDict and not
                # a plain Dict. The primary difference is that the QueryDict
                # stores every value in a list and is, by default, immutable.
                # The primary issue is making sure that list values are properly
                # inserted into the QueryDict. If we simply do a q_data.update(data),
                # any list values will be wrapped in another list. By iterating
                # through the list and updating for each value, we get the
                # expected result of a single list.
                q_data = QueryDict('', mutable=True)
                for key, value in data.items():
                    if isinstance(value, list):
                        # Iterate through the list and update q_data so the list
                        # does not get wrapped in another list.
                        for x in value:
                            q_data.update({key: x})
                    else:
                        q_data.update({key: value})
            else:
                # Entirely inconsistent, but we keep the data this way
                q_data = data

            if request.method == 'GET':
                request.GET = q_data

            if request.method in ('POST', 'PATCH', 'PUT'):
                request.POST = q_data

        return self._response(request)
