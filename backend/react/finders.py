import os.path
from django.conf import settings
from django.contrib.staticfiles.finders import BaseFinder
from django.contrib.staticfiles.utils import matches_patterns
from django.core.files.storage import FileSystemStorage
from django.core.checks import Error
from .utils import read_manifest

__all__ = 'AssetManifestFinder',

EXTRA_STATICS = ('service-worker.js',)


# This uses internal interfaces, so it might break
class AssetManifestFinder(BaseFinder):
    def __init__(self):
        # assets manifest works in absolute paths, but we need to be relative to STATIC_URL
        self.prefix = settings.STATIC_URL.lstrip('/')
        self.location = settings.REACT_BUILD_DIR
        self.storage = FileSystemStorage(os.path.join(self.location, self.prefix))
        self.assetmanifest = os.path.join(self.location, 'asset-manifest.json')
        self.special_storage = FileSystemStorage(os.path.join(self.location))

    _assets = None

    @property
    def assets(self):
        if self._assets is None:
            self._assets = {
                v: (r, self.storage)
                for v, r in read_manifest(self.assetmanifest).items()
            }

            # And some extra files
            for fn in EXTRA_STATICS:
                self.assets[fn] = (fn, self.special_storage)

        return self._assets

    def reloadAssets(self):
        self._assets = None

    def check(self, **kwargs):
        errors = []
        if not os.path.exists(self.assetmanifest):
            errors.append(Error(
                'Missing asset-manifest.json',
                hint='Misconfigured REACT_BUILD_DIR or did not run the frontend build?',
                id='staticfiles_manifest.E001',
            ))
        return errors

    # Used by {% static %}, findstatic, DEBUG static file serving
    def find(self, path, all=False):
        if settings.DEBUG:
            self.reloadAssets()
        matches = []
        realpath, storage = self.assets.get(path, (None, None))
        if realpath is None:
            # Check to see if we were handed the compiled name
            for realpath, storage in self.assets.values():
                if realpath == path:
                    break
            else:
                realpath = storage = None
        if realpath:
            realpath = os.path.join(storage.location, realpath)
        if all:
            if realpath:
                matches.append(realpath)
            return matches
        else:
            if realpath:
                return realpath
            else:
                # I'm not sure why an empty list in this case, but that seems to be required
                return []

    # Used by collectstatic
    def list(self, ignore_patterns):
        for vpath, (rpath, storage) in self.assets.items():
            if matches_patterns(vpath, ignore_patterns):
                continue
            yield rpath, storage
