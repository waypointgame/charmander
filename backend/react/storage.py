from whitenoise.storage import CompressedManifestStaticFilesStorage
import re
from .utils import read_manifest


HASHED_SECTION = re.compile(r'\.[0-9a-fA-F]{8,}\.')


class CMSFStorage_Manifest(CompressedManifestStaticFilesStorage):
    def __init__(self, *p, **kw):
        super().__init__(*p, **kw)
        self.asset_manifest = read_manifest()

    def hashed_name(self, name, content=None, filename=None):
        """
        If it looks like it's been hashed already, don't rehash it.
        """
        if HASHED_SECTION.search(name):
            return name
        else:
            return super().hashed_name(name, content, filename)

    def save_manifest(self):
        # Add frontend logical names to the saved files before saving
        for v, r in self.asset_manifest.items():
            self.hashed_files[v] = r
        super().save_manifest()

    def post_process(self, paths, dry_run=False, **options):
        # Strip out frontend files, don't rewrite them
        paths = {
            fn: v
            for fn, v in paths.items()
            if fn not in self.asset_manifest.values()
        }
        yield from super().post_process(paths, dry_run=False, **options)
