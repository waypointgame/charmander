import os
import json
from django.conf import settings
from typing import Optional


def read_manifest(manifest: Optional[str]=None, prefix: Optional[str]=None):
    if manifest is None:
        manifest = os.path.join(settings.REACT_BUILD_DIR, 'asset-manifest.json')
    if prefix is None:
        prefix = settings.STATIC_URL.lstrip('/')
    with open(manifest, 'rt') as fp:
        return {
            v: r[len(prefix):]
            for v, r in json.load(fp).items()
            if r.startswith(prefix)
        }
