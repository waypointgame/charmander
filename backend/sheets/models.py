from django.db import models
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.contrib.postgres import fields as pfields
from model_utils.models import SoftDeletableModel, TimeStampedModel
from model_utils import FieldTracker


class AbstractSheet(models.Model):
    schema = models.CharField(max_length=32)
    data = pfields.JSONField(default={})

    class Meta:
        abstract = True


class SheetLockedError(PermissionDenied):
    """
    Attempted to edit a locked sheet without sufficient permissions.
    """


class Sheet(AbstractSheet, TimeStampedModel, SoftDeletableModel):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL)

    class Meta:
        permissions = [
            ("view_sheet", "Can View"),
        ]

    tracker = FieldTracker()

    def snapshot(self, *, user=None):
        if user is not None:
            if not user.has_perm('sheets.can_view', self):
                raise PermissionDenied
        assert self.pk is not None
        snap = Snapshot(
            sheet=self,
            **{
                fname: getattr(self, fname)
                for fname in (f.name for f in AbstractSheet._meta.get_fields())
            }
        )
        snap.save(user=user)
        return snap

    def check_save_perms(self, user):
        if not user.has_perm('sheets.can_view', self):
            raise PermissionDenied
        if not user.has_perm('sheets.change_sheet', self):
            raise PermissionDenied

    def save(self, *, user=None):
        if user is not None:
            self.check_save_perms()
        super().save()

    def _largest_vnum(self):
        return self.snapshots.aggregate(models.Max('vnum'))['vnum__max'] or 0


class SnapshotMutationError(PermissionDenied):
    """
    Tried to change a snapshot.
    """


class Snapshot(AbstractSheet):
    sheet = models.ForeignKey(Sheet)
    created = models.DatetimeField(auto_now_add=True)
    vnum = models.PositiveSmallIntegerField(blank=True)  # Actually, default set in .save()

    class Meta:
        permissions = [
            ("view_snapshot", "Can View"),
        ]

    def save(self, *, user=None):
        if self.pk is not None:
            raise SnapshotMutationError
        if self.vnum is None:
            self.vnum = self.sheet._largest_vnum() + 1
        if user is not None:
            if not user.has_perm('sheets.add_snapshot', self):
                raise PermissionDenied
        super().save()
