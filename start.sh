#!/bin/sh
python /opt/charmander/backend/manage.py migrate
python /opt/charmander/backend/manage.py initsuperuser
gunicorn -c /opt/charmander/ops/guni-docker.cfg charmander.wsgi