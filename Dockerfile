FROM python:3.6-slim-stretch

WORKDIR /opt/charmander

COPY . /opt/charmander

RUN apt-get update && apt-get install --yes salt-minion git && \
    salt-call --local --retcode-passthrough -c /opt/charmander/salt state.apply container && \
    apt-get purge --yes salt-minion git && \
    apt-get autoremove --yes && \
    apt-get clean --yes && rm -rf /var/lib/apt/lists/*

EXPOSE 5000
CMD ["/opt/charmander/start.sh"]
